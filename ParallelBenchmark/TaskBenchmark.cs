using System;
using BenchmarkDotNet;
using BenchmarkDotNet.Attributes;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ParallelBenchmark
{
    [Config(typeof(BenchmarkConfig))] 
    public class TaskBenchmark
    {
        IEnumerable<int> ListNumbers;

        [Params(100_000, 1_000_000, 10_000_000)]
        public int N;

        [GlobalSetup]
        public void Setup()
        {
            ListNumbers = new int[N];
        }
        
        [Benchmark(Baseline = true)]
        public void Base()
        {
           ListNumbers.Sum();
        }
        [Benchmark]
        public async Task<int> Parallel()
        {
            var processCount = Environment.ProcessorCount;
            var count = ListNumbers.Count();
            var range = count / processCount;
            var tasks = new List<Task<int>>();

            for (var i = 0; i < processCount; i++)
            {
                var i1 = i;
                tasks.Add(Task.Run(() => ListNumbers.Skip(range * i1).Sum()));
            }

            await Task.WhenAll();
            return tasks.Select(task => task.Result).Sum();
        }

        [Benchmark]
        public void ParallelWithLinq()
        {
            ListNumbers.AsParallel().Sum();
        }
    }
}