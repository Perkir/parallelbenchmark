﻿using System;
using System.Collections.Generic;
using System.Linq;
using BenchmarkDotNet.Running;

namespace ParallelBenchmark
{
    class Program
    {
        static void Main(string[] args)
        { 
            var summary = BenchmarkRunner.Run<TaskBenchmark>();
        }
    }
}